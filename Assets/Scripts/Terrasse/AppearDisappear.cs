﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearDisappear : MonoBehaviour
{

    public GameObject[] boats;
    public Transform[] spawns;
    public bool[] isDisappearing;
    public bool[] isAppearing;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < boats.Length; i++)
        {
            if (Vector3.Distance(boats[i].transform.position, spawns[i].position) > 430)
            {
                if (isDisappearing[i])
                {
                    boats[i].transform.localScale = boats[i].transform.localScale - new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 1f * Time.deltaTime);
                }

                if (isDisappearing[i] == false)
                {
                    isDisappearing[i] = true;
                    StartCoroutine(Disappear(i));
                }

            }

            if (isAppearing[i])
            {
                boats[i].transform.localScale = boats[i].transform.localScale + new Vector3(1f * Time.deltaTime, 1f * Time.deltaTime, 1f * Time.deltaTime);

                if (boats[i].transform.localScale.x >= 1f)
                {
                    isAppearing[i] = false;
                    boats[i].transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }    
    }

    public IEnumerator Disappear(int i)
    {
        yield return new WaitForSeconds(1f);
        isAppearing[i] = true;
        boats[i].transform.position = spawns[i].position;
        isDisappearing[i] = false;
    }
}
