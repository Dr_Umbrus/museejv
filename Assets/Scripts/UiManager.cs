﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    private GameObject options;
    private GameObject menuMaps;
    private GameObject credits;

    private int etageActuel = 0;

    public Image[] maps;
    public Image[] etages;
    public Image etageChoisi;
    public Image creditsText;

    public Transform creditsScroll;

    public Material[] materials;
    private float dissolveAmount;
    public float speedScroll;

    public bool dissolveRemoving = false;
    public bool dissolveCreating = false;
    public bool isScrolling = false;

    // Start is called before the first frame update
    void Start()
    {
        options = GameObject.FindGameObjectWithTag("Options");
        options.SetActive(false);
        menuMaps = GameObject.FindGameObjectWithTag("Maps");
        menuMaps.SetActive(false);
        credits = GameObject.FindGameObjectWithTag("Credits");
        credits.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (dissolveRemoving)
        {
            dissolveAmount = Mathf.Clamp01(dissolveAmount + Time.deltaTime);
            materials[0].SetFloat("_dissolveAmount", dissolveAmount);
            materials[1].SetFloat("_dissolveAmount", dissolveAmount);
            materials[2].SetFloat("_dissolveAmount", dissolveAmount);

            if(dissolveAmount >= 1)
            {
                dissolveRemoving = false;
                dissolveCreating = true;
            }
        }

        if (dissolveCreating)
        {
            for (int i = 0; i < maps.Length; i++)
            {
                maps[i].gameObject.SetActive(false);
            }

            maps[etageActuel].gameObject.SetActive(true);

            dissolveAmount = Mathf.Clamp01(dissolveAmount - Time.deltaTime);
            materials[etageActuel].SetFloat("_dissolveAmount", dissolveAmount);

            if(dissolveAmount <= 0)
            {
                dissolveCreating = false;
            }
        }

        if (isScrolling)
        {
            creditsText.transform.position += transform.up * Time.deltaTime * speedScroll;
        }
    }

    public void OnClickOptions()
    {
        if (options.activeSelf)
        {
            options.SetActive(false);
        }
        else if (options.activeSelf == false)
        {
            options.SetActive(true);
        }
    }

    public void OnClickMap()
    {
        if (menuMaps.activeSelf)
        {
            menuMaps.SetActive(false);
        }
        else if (menuMaps.activeSelf == false)
        {
            menuMaps.SetActive(true);
        }
    }
    public void OnClickCredits()
    {
        if (credits.activeSelf)
        {
            credits.SetActive(false);
            isScrolling = false;
        }
        else if (credits.activeSelf == false)
        {
            credits.SetActive(true);
            creditsText.transform.position = creditsScroll.position;
            isScrolling = true;
        }
    }

    public void OnClickEtage0()
    {
        dissolveRemoving = true;
        dissolveCreating = false;

        /*maps[0].gameObject.SetActive(true);
        maps[1].gameObject.SetActive(false);
        maps[2].gameObject.SetActive(false);*/

        etageChoisi.transform.position = etages[0].transform.position;
        etageActuel = 0;
    }

    public void OnClickEtage1()
    {
        dissolveRemoving = true;
        dissolveCreating = false;

        /*maps[0].gameObject.SetActive(false);
        maps[1].gameObject.SetActive(true);
        maps[2].gameObject.SetActive(false);*/

        etageChoisi.transform.position = etages[1].transform.position;
        etageActuel = 1;
    }

    public void OnClickEtage2()
    {
        dissolveRemoving = true;
        dissolveCreating = false;

        /*maps[0].gameObject.SetActive(false);
        maps[1].gameObject.SetActive(false);
        maps[2].gameObject.SetActive(true);*/

        etageChoisi.transform.position = etages[2].transform.position;
        etageActuel = 2;
    }
}
